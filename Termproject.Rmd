---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.11.4
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```{python}
# %autosave 0
```

```{python}
#pip install pandas
#pip install pandasql
 #pip install oauth2client gspread
```

```{python}
import pandas as pd
import pandasql
```

```{python}
from oauth2client.service_account import ServiceAccountCredentials
import gspread

```

```{python}
url = 'https://docs.google.com/spreadsheets/d/1_y_5021I82FkS8Dt2WNxuM1RnDC-EPWSVmzhy0TDzvw/edit?usp=sharing'
```

```{python}
scope = ['https://www.googleapis.com/auth/spreadsheets']
credentials = ServiceAccountCredentials.from_json_keyfile_name('my_json_file.json', scope)
client = gspread.authorize(credentials)
sheet = client.open_by_url(url)
worksheet = sheet.get_worksheet(0)
results = worksheet.get_all_records()
```

```{python}
result_df = pd.DataFrame(results)
```

```{python}
result_df.head()
```

```{python}
result_df.columns

```

```{python}
result_df.dtypes
```

```{python}
len(result_df.columns)

```

```{python}
result_df.shape
```

```{python}
result_df.columns.to_list()
column_name = result_df.columns.to_list()
column_name
```

```{python}
result_df.loc[result_df.ข้อเสนอแนะ == '-', 'ข้อเสนอแนะ'] = 'ไม่มี'

```

```{python}
result_df.loc[result_df.ข้อเสนอแนะ == '', 'ข้อเสนอแนะ'] = 'ไม่มี'

```

```{python}
cleanup_nums = {"นักศึกษาใช้ทางม้าลายหน้ามหาวิทยาลัยเพื่อข้ามถนนบ่อยแค่ไหน":     {"บ่อยมาก": 4, "บ่อย": 3, "บางครั้ง":2,"ไม่เคย":1},
                "นักศึกษาหรือคนรอบตัวเคยประสบอุบัติเหตุจากการข้ามถนนหน้ามหาวิทยาลัย": {"เคย": 2, "ไม่เคย": 1},
                "นักศึกษาคิดว่าทางม้าลายหน้ามหาวิทยาลัยปลอดภัยเวลาข้ามถนน": {"ปลอดภัย":4, "ค่อนข้างปลอดภัย":3,"ไม่ค่อยปลอดภัย":2,"ไม่ปลอดภัย":1},
                "นักศึกษาคิดว่าควรมีสะพานลอยหรืออุโมงค์ในการข้ามถนนหรือไม่": {"ควรมี":2,"ไม่ควรมี":1}}
```

```{python}
result_df = result_df.replace(cleanup_nums)
result_df.head()
```

```{python}
result_df
```

```{python}
result_df.to_csv('result.csv', encoding='utf8', columns = column_name, index=False)

```
